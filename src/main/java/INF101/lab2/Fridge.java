package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
    
    int max_size;
    List<FridgeItem> items_in_fridge;

    // Constructor

    public Fridge () {
        max_size = 20;
        items_in_fridge = new ArrayList <FridgeItem>();
    }


    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return items_in_fridge.size();

    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items_in_fridge.size() < 20) {
            items_in_fridge.add(item);
            return true;
        }
            else return false;    
    }


    @Override
    public void takeOut(FridgeItem item) {
        if (!(items_in_fridge.contains(item))) {
            throw new NoSuchElementException ();
        }
        else {
            items_in_fridge.remove(item);
        }
    }



    @Override
    public void emptyFridge() {
        items_in_fridge.clear();        
    }


    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired_item = new ArrayList<FridgeItem>();

            for (FridgeItem item : items_in_fridge){

                if (item.hasExpired()){
                    expired_item.add(item);
                }
            }

            items_in_fridge.removeAll(expired_item);
            return expired_item;
        }

    
    
    



}
